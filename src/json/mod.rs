use crate::{Message, MessageType, SignalRCommunicator};
use serde::Serialize;
use std::io::{Read, Write};

#[derive(Serialize)]
struct JsonMessage {
    #[serde(rename = "type")]
    message_type: MessageType,
    #[serde(flatten, skip_serializing_if = "is_ping")]
    message: Message,
}

fn is_ping(m: &Message) -> bool {
    if let Message::Ping = m {
        true
    } else {
        false
    }
}

pub struct SignalRJson;
mod read;
mod write;

impl SignalRCommunicator for SignalRJson {
    fn read<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
        read::read(r)
    }

    fn write<W: Write>(w: &mut W, message: Message) -> Result<(), Box<dyn std::error::Error>> {
        write::write(w, message)
    }
}
