use super::JsonMessage;
use crate::{Message, MessageType};
use std::io::Write;

pub(super) fn write<W: Write>(
    w: &mut W,
    message: Message,
) -> Result<(), Box<dyn std::error::Error>> {
    let message_type = match message {
        Message::Invocation(_) => MessageType::Invocation,
        Message::StreamInvocation(_) => MessageType::StreamInvocation,
        Message::StreamItem(_) => MessageType::StreamItem,
        Message::Completion(_) => MessageType::Completion,
        Message::CancelInvocation(_) => MessageType::CancelInvocation,
        Message::Ping => MessageType::Ping,
        Message::Close(_) => MessageType::Close,
    };

    let json_message = JsonMessage {
        message_type,
        message,
    };
    let mut buffer = serde_json::to_vec(&json_message)?;
    buffer.push(0x1E);
    w.write_all(&buffer)?;

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::write;
    use crate::{
        CancelInvocation, Close, Completion, Invocation, Message, ResultKind, StreamInvocation,
        StreamItem,
    };
    use std::collections::HashMap;

    #[test]
    fn non_blocking_invocation() {
        let value = format!(
            "{}\x1E",
            r#"{"type":1,"target":"Send","arguments":[42,"Test Message"]}"#
        );

        let invocation = Message::Invocation(Invocation {
            headers: HashMap::new(),
            id: None,
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn invocation_with_headers() {
        let value = format!(
            "{}\x1E",
            r#"{"type":1,"headers":{"x":"y"},"target":"Send","arguments":[42,"Test Message"]}"#
        );

        let mut headers = HashMap::new();
        headers.insert("x".to_string(), "y".to_string());
        let invocation = Message::Invocation(Invocation {
            headers,
            id: None,
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn invocation_with_stream_ids() {
        let value = format!(
            "{}\x1E",
            r#"{"type":1,"target":"Send","arguments":[42,"Test Message"],"streamIds":["1"]}"#
        );

        let invocation = Message::Invocation(Invocation {
            headers: HashMap::new(),
            id: None,
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: Some(vec!["1".into()]),
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn blocking_invocation() {
        let value = format!(
            "{}\x1E",
            r#"{"type":1,"invocationId":"123","target":"Send","arguments":[42,"Test Message"]}"#
        );

        let invocation = Message::Invocation(Invocation {
            headers: HashMap::new(),
            id: Some("123".into()),
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn stream_invocation() {
        let value = format!(
            "{}\x1E",
            r#"{"type":4,"invocationId":"123","target":"Send","arguments":[42,"Test Message"]}"#
        );

        let invocation = Message::StreamInvocation(StreamInvocation {
            headers: HashMap::new(),
            id: "123".into(),
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn stream_item() {
        let value = format!("{}\x1E", r#"{"type":2,"invocationId":"123","item":42}"#);

        let invocation = Message::StreamItem(StreamItem {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            item: rmpv::Value::from(42),
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn void_completion() {
        let value = format!("{}\x1E", r#"{"type":3,"invocationId":"123"}"#);

        let invocation = Message::Completion(Completion {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            result_kind: ResultKind::Void,
            result: None,
            error: None,
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn non_void_completion() {
        let value = format!("{}\x1E", r#"{"type":3,"invocationId":"123","result":42}"#);

        let invocation = Message::Completion(Completion {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            result_kind: ResultKind::Void,
            result: Some(rmpv::Value::from(42)),
            error: None,
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn error_completion() {
        let value = format!(
            "{}\x1E",
            r#"{"type":3,"invocationId":"123","error":"This is not good"}"#
        );

        let invocation = Message::Completion(Completion {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            result_kind: ResultKind::Void,
            result: None,
            error: Some("This is not good".into()),
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn cancel_invocation() {
        let value = format!("{}\x1E", r#"{"type":5,"invocationId":"123"}"#);

        let invocation = Message::CancelInvocation(CancelInvocation {
            headers: HashMap::new(),
            invocation_id: "123".into(),
        });

        let mut buf = Vec::new();
        write(&mut buf, invocation).unwrap();

        assert_eq!(value, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn ping() {
        let message = Message::Ping;
        let expected = format!("{}\x1E", r#"{"type":6}"#);

        let mut buf = Vec::new();
        write(&mut buf, message).unwrap();

        assert_eq!(expected, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn close_no_error() {
        let message = Message::Close(Close {
            error: None,
            allow_reconnect: None,
        });
        let expected = format!("{}\x1E", r#"{"type":7}"#);

        let mut buf = Vec::new();
        write(&mut buf, message).unwrap();

        assert_eq!(expected, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn close_with_error() {
        let message = Message::Close(Close {
            error: Some("xyz".into()),
            allow_reconnect: None,
        });
        let expected = format!("{}\x1E", r#"{"type":7,"error":"xyz"}"#);

        let mut buf = Vec::new();
        write(&mut buf, message).unwrap();

        assert_eq!(expected, std::str::from_utf8(&buf).unwrap());
    }

    #[test]
    fn close_with_allow_reconnect() {
        let message = Message::Close(Close {
            error: Some("xyz".into()),
            allow_reconnect: Some(true),
        });
        let expected = format!(
            "{}\x1E",
            r#"{"type":7,"error":"xyz","allowReconnect":true}"#
        );

        let mut buf = Vec::new();
        write(&mut buf, message).unwrap();

        assert_eq!(expected, std::str::from_utf8(&buf).unwrap());
    }
}
