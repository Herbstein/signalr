use super::JsonMessage;
use crate::{
    CancelInvocation, Close, Completion, Invocation, Message, MessageType, StreamInvocation,
    StreamItem,
};
use serde::de::{self, Deserializer, IntoDeserializer, MapAccess, Visitor};
use serde::Deserialize;
use std::collections::HashMap;
use std::{fmt, io::Read};

impl<'de> Deserialize<'de> for JsonMessage {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "camelCase")]
        enum Field {
            Type,
            Headers,
            InvocationId,
            Target,
            Arguments,
            Item,
            Result,
            Error,
            AllowReconnect,
        };

        impl fmt::Display for Field {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                match *self {
                    Field::Type => write!(f, "type"),
                    Field::Headers => write!(f, "headers"),
                    Field::InvocationId => write!(f, "invocationId"),
                    Field::Target => write!(f, "target"),
                    Field::Arguments => write!(f, "arguments"),
                    Field::Item => write!(f, "item"),
                    Field::Result => write!(f, "result"),
                    Field::Error => write!(f, "error"),
                    Field::AllowReconnect => write!(f, "allowReconnect"),
                }
            }
        }

        struct JsonMessageVisitor;

        impl<'de> Visitor<'de> for JsonMessageVisitor {
            type Value = JsonMessage;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct JsonMessage")
            }

            fn visit_map<V>(self, mut map: V) -> Result<JsonMessage, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut message_type = None;
                let mut other_items: HashMap<String, serde_json::Value> = HashMap::new();
                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Type => {
                            message_type = Some(map.next_value()?);
                        }
                        _ => {
                            other_items.insert(key.to_string(), map.next_value()?);
                        }
                    }
                }

                let message_type = message_type.ok_or_else(|| de::Error::missing_field("type"))?;
                let mvd = other_items.into_deserializer();
                let message = match message_type {
                    MessageType::Invocation => {
                        Message::Invocation(Invocation::deserialize(mvd).unwrap())
                    }
                    MessageType::StreamInvocation => {
                        Message::StreamInvocation(StreamInvocation::deserialize(mvd).unwrap())
                    }
                    MessageType::StreamItem => {
                        Message::StreamItem(StreamItem::deserialize(mvd).unwrap())
                    }
                    MessageType::Completion => {
                        Message::Completion(Completion::deserialize(mvd).unwrap())
                    }
                    MessageType::CancelInvocation => {
                        Message::CancelInvocation(CancelInvocation::deserialize(mvd).unwrap())
                    }
                    MessageType::Ping => Message::Ping,
                    MessageType::Close => Message::Close(Close::deserialize(mvd).unwrap()),
                };

                Ok(JsonMessage {
                    message_type,
                    message,
                })
            }
        }

        const FIELDS: &[&str] = &[
            "type",
            "headers",
            "invocationId",
            "target",
            "arguments",
            "item",
            "result",
            "error",
            "allowReconnect",
        ];
        deserializer.deserialize_struct("JsonMessage", FIELDS, JsonMessageVisitor)
    }
}

pub(super) fn read<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let json_message: JsonMessage = serde_json::from_reader(r)?;

    Ok(json_message.message)
}

#[cfg(test)]
mod tests {
    use super::read;
    use crate::{
        json::SignalRJson, CancelInvocation, Close, Completion, Invocation, Message, ResultKind,
        SignalRCommunicator, StreamInvocation, StreamItem,
    };
    use std::collections::HashMap;

    #[test]
    fn non_blocking_invocation() {
        let value = r#"{"target":"Send","type":1,"arguments":[42,"Test Message"]}"#;

        let expected = Message::Invocation(Invocation {
            headers: HashMap::new(),
            id: None,
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn invocation_with_headers() {
        let value =
            r#"{"type":1,"headers":{"x":"y"},"target":"Send","arguments":[42,"Test Message"]}"#;

        let mut headers = HashMap::new();
        headers.insert("x".to_string(), "y".to_string());
        let expected = Message::Invocation(Invocation {
            headers,
            id: None,
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn blocking_invocation() {
        let value =
            r#"{"type":1,"invocationId":"123","target":"Send","arguments":[42,"Test Message"]}"#;

        let expected = Message::Invocation(Invocation {
            headers: HashMap::new(),
            id: Some("123".into()),
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn stream_invocation() {
        let value =
            r#"{"type":4,"invocationId":"123","target":"Send","arguments":[42,"Test Message"]}"#;

        let expected = Message::StreamInvocation(StreamInvocation {
            headers: HashMap::new(),
            id: "123".into(),
            target: "Send".into(),
            arguments: vec![rmpv::Value::from(42), rmpv::Value::from("Test Message")],
            stream_ids: None,
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn stream_item() {
        let value = r#"{"type":2,"invocationId":"123","item":42}"#;

        let expected = Message::StreamItem(StreamItem {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            item: rmpv::Value::from(42),
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn void_completion() {
        let value = r#"{"type":3,"invocationId":"123"}"#;

        let expected = Message::Completion(Completion {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            result_kind: ResultKind::Void,
            result: None,
            error: None,
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn non_void_completion() {
        let value = r#"{"type":3,"invocationId":"123","result":42}"#;

        let expected = Message::Completion(Completion {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            result_kind: ResultKind::Void,
            result: Some(rmpv::Value::from(42)),
            error: None,
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn error_completion() {
        let value = r#"{"type":3,"invocationId":"123","error":"This is not good"}"#;

        let expected = Message::Completion(Completion {
            headers: HashMap::new(),
            invocation_id: "123".into(),
            result_kind: ResultKind::Void,
            result: None,
            error: Some("This is not good".into()),
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn cancel_invocation() {
        let value = r#"{"type":5,"invocationId":"123"}"#;

        let expected = Message::CancelInvocation(CancelInvocation {
            headers: HashMap::new(),
            invocation_id: "123".into(),
        });

        let mut buf = value.as_bytes();
        let message = SignalRJson::read(&mut buf).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn ping() {
        let value = r#"{"type":6}"#;
        let mut buf = value.as_bytes();

        if let Message::Ping = read(&mut buf).unwrap() {
            assert!(true);
        }
    }

    #[test]
    fn close_with_allow_reconnect() {
        let value = r#"{"type":7,"allowReconnect":true}"#;
        let mut buf = value.as_bytes();

        let expected = Message::Close(Close {
            error: None,
            allow_reconnect: Some(true),
        });

        let message = read(&mut buf).unwrap();
        assert_eq!(expected, message);
    }

    #[test]
    fn close() {
        let value = r#"{"type":7}"#;
        let mut buf = value.as_bytes();

        let expected = Message::Close(Close {
            error: None,
            allow_reconnect: None,
        });

        let message = read(&mut buf).unwrap();
        assert_eq!(expected, message);
    }

    #[test]
    fn close_with_error() {
        let value = r#"{"type":7,"error":"xyz"}"#;
        let mut buf = value.as_bytes();

        let expected = Message::Close(Close {
            error: Some("xyz".into()),
            allow_reconnect: None,
        });

        let message = read(&mut buf).unwrap();
        assert_eq!(expected, message);
    }
}
