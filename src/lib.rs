use rmpv::Value;
use serde::{Deserialize, Serialize};
use serde_repr::{Deserialize_repr, Serialize_repr};
use std::{
    collections::HashMap,
    convert::TryInto,
    io::{Read, Write},
};

#[repr(u8)]
#[derive(Serialize_repr, Deserialize_repr, PartialEq, Debug)]
pub enum MessageType {
    Invocation = 1,
    StreamItem = 2,
    Completion = 3,
    StreamInvocation = 4,
    CancelInvocation = 5,
    Ping = 6,
    Close = 7,
}

impl TryInto<MessageType> for u8 {
    type Error = Box<dyn std::error::Error>;

    fn try_into(self) -> Result<MessageType, Self::Error> {
        match self {
            1 => Ok(MessageType::Invocation),
            2 => Ok(MessageType::StreamItem),
            3 => Ok(MessageType::Completion),
            4 => Ok(MessageType::StreamInvocation),
            5 => Ok(MessageType::CancelInvocation),
            6 => Ok(MessageType::Ping),
            7 => Ok(MessageType::Close),
            _ => Err(format!("'{}' isn't a `MessageType`", self).into()),
        }
    }
}

#[repr(u8)]
#[derive(Debug, PartialEq)]
pub enum ResultKind {
    Error = 1,
    Void = 2,
    NonVoid = 3,
}

impl std::default::Default for ResultKind {
    fn default() -> Self {
        ResultKind::Void
    }
}

impl TryInto<ResultKind> for u8 {
    type Error = Box<dyn std::error::Error>;

    fn try_into(self) -> Result<ResultKind, Self::Error> {
        match self {
            1 => Ok(ResultKind::Error),
            2 => Ok(ResultKind::Void),
            3 => Ok(ResultKind::NonVoid),
            _ => Err(format!("'{}' isn't a `ResultKind`", self).into()),
        }
    }
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(untagged)]
pub enum Message {
    Invocation(Invocation),
    StreamInvocation(StreamInvocation),
    StreamItem(StreamItem),
    Completion(Completion),
    CancelInvocation(CancelInvocation),
    Ping,
    Close(Close),
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct Invocation {
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub headers: HashMap<String, String>,
    #[serde(rename = "invocationId", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    pub target: String,
    pub arguments: Vec<Value>,
    #[serde(rename = "streamIds", skip_serializing_if = "Option::is_none")]
    pub stream_ids: Option<Vec<String>>,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
pub struct StreamInvocation {
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub headers: HashMap<String, String>,
    #[serde(rename = "invocationId")]
    pub id: String,
    pub target: String,
    pub arguments: Vec<Value>,
    #[serde(rename = "streamIds", skip_serializing_if = "Option::is_none")]
    pub stream_ids: Option<Vec<String>>,
}

impl Into<Invocation> for StreamInvocation {
    fn into(self) -> Invocation {
        Invocation {
            headers: self.headers,
            id: Some(self.id),
            target: self.target,
            arguments: self.arguments,
            stream_ids: self.stream_ids,
        }
    }
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct StreamItem {
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub headers: HashMap<String, String>,
    pub invocation_id: String,
    pub item: Value,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Completion {
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub headers: HashMap<String, String>,
    pub invocation_id: String,
    #[serde(skip)]
    pub result_kind: ResultKind,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub result: Option<Value>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<String>,
}

#[derive(Debug, PartialEq, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CancelInvocation {
    #[serde(default, skip_serializing_if = "HashMap::is_empty")]
    pub headers: HashMap<String, String>,
    pub invocation_id: String,
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Close {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub allow_reconnect: Option<bool>,
}

pub trait SignalRCommunicator {
    fn read<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>>;
    fn write<W: Write>(w: &mut W, message: Message) -> Result<(), Box<dyn std::error::Error>>;
}

pub mod json;
pub mod message_pack;
