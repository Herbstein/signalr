use crate::{CancelInvocation, Close, Completion, Invocation, Message, MessageType, StreamItem};
use rmp::encode;
use std::{collections::HashMap, io::Write};

pub(super) fn write<W: Write>(
    w: &mut W,
    message: Message,
) -> Result<(), Box<dyn std::error::Error>> {
    let mut buf = Vec::new();

    match message {
        Message::Invocation(invocation) => write_standard_invocation(&mut buf, invocation)?,
        Message::StreamInvocation(invocation) => {
            write_stream_invocation(&mut buf, invocation.into())?
        }
        Message::StreamItem(item) => write_stream_item(&mut buf, item)?,
        Message::Completion(item) => write_completion(&mut buf, item)?,
        Message::CancelInvocation(item) => write_cancel_invocation(&mut buf, item)?,
        Message::Ping => write_ping(&mut buf)?,
        Message::Close(s) => write_close(&mut buf, s)?,
    };

    write_frame(w, buf)
}

fn write_standard_invocation<W: Write>(
    w: &mut W,
    invocation: Invocation,
) -> Result<(), Box<dyn std::error::Error>> {
    write_invocation(w, invocation, MessageType::Invocation)
}

fn write_stream_invocation<W: Write>(
    w: &mut W,
    invocation: Invocation,
) -> Result<(), Box<dyn std::error::Error>> {
    write_invocation(w, invocation, MessageType::StreamInvocation)
}

fn write_invocation<W: Write>(
    w: &mut W,
    invocation: Invocation,
    invocation_type: MessageType,
) -> Result<(), Box<dyn std::error::Error>> {
    encode::write_array_len(w, 5)?;
    encode::write_pfix(w, invocation_type as u8)?;
    write_headers(w, invocation.headers)?;

    if let Some(invocation_id) = invocation.id {
        encode::write_str(w, &*invocation_id)?;
    } else {
        encode::write_nil(w)?;
    }

    encode::write_str(w, &*invocation.target)?;
    encode::write_array_len(w, invocation.arguments.len() as u32)?;
    for argument in invocation.arguments {
        rmpv::encode::write_value(w, &argument)?;
    }

    if let Some(stream_ids) = invocation.stream_ids {
        encode::write_array_len(w, stream_ids.len() as u32)?;
        for stream_id in stream_ids {
            encode::write_str(w, &stream_id)?;
        }
    } else {
        encode::write_array_len(w, 0)?;
    }

    Ok(())
}

fn write_stream_item<W: Write>(
    w: &mut W,
    item: StreamItem,
) -> Result<(), Box<dyn std::error::Error>> {
    encode::write_array_len(w, 4)?;
    encode::write_pfix(w, MessageType::StreamItem as u8)?;
    write_headers(w, item.headers)?;
    encode::write_str(w, &*item.invocation_id)?;
    rmpv::encode::write_value(w, &item.item)?;

    Ok(())
}

fn write_completion<W: Write>(
    w: &mut W,
    item: Completion,
) -> Result<(), Box<dyn std::error::Error>> {
    let message_array_len = if let Some(_) = item.result { 5 } else { 4 };
    encode::write_array_len(w, message_array_len)?;
    encode::write_pfix(w, MessageType::Completion as u8)?;
    write_headers(w, item.headers)?;
    encode::write_str(w, &*item.invocation_id)?;
    encode::write_pfix(w, item.result_kind as u8)?;

    if let Some(value) = item.result {
        rmpv::encode::write_value(w, &value)?;
    }

    Ok(())
}

fn write_cancel_invocation<W: Write>(
    w: &mut W,
    item: CancelInvocation,
) -> Result<(), Box<dyn std::error::Error>> {
    encode::write_array_len(w, 3)?;
    encode::write_pfix(w, MessageType::CancelInvocation as u8)?;
    write_headers(w, item.headers)?;
    encode::write_str(w, &*item.invocation_id)?;

    Ok(())
}

fn write_ping<W: Write>(w: &mut W) -> Result<(), Box<dyn std::error::Error>> {
    encode::write_array_len(w, 1)?;
    encode::write_pfix(w, MessageType::Ping as u8)?;

    Ok(())
}

fn write_close<W: Write>(w: &mut W, item: Close) -> Result<(), Box<dyn std::error::Error>> {
    let array_len = if let Some(_) = item.allow_reconnect {
        3
    } else {
        2
    };
    encode::write_array_len(w, array_len)?;
    encode::write_pfix(w, MessageType::Close as u8)?;

    match item.error {
        Some(error) => encode::write_str(w, &*error)?,
        None => encode::write_str(w, "")?,
    };

    match item.allow_reconnect {
        Some(allow_reconnect) => encode::write_bool(w, allow_reconnect)?,
        None => (),
    };

    Ok(())
}

fn write_headers<W: Write>(
    w: &mut W,
    headers: HashMap<String, String>,
) -> Result<(), Box<dyn std::error::Error>> {
    encode::write_map_len(w, headers.len() as u32)?;
    for (key, value) in headers {
        encode::write_str(w, &*key)?;
        encode::write_str(w, &*value)?;
    }

    Ok(())
}

fn write_frame<W: Write>(w: &mut W, mut buffer: Vec<u8>) -> Result<(), Box<dyn std::error::Error>> {
    let len_as_var_int: u64 = convert_to_var_int(buffer.len());

    encode::write_uint(w, len_as_var_int)?;
    w.write_all(&mut buffer)?;
    Ok(())
}

fn convert_to_var_int(value: usize) -> u64 {
    let mut length = value;
    let mut len_as_var_int: u64 = 0;
    while length >> 7 != 0 {
        let least_significant_byte = (length & 0x7f | 0x80) as u64;

        len_as_var_int = len_as_var_int << 8;
        len_as_var_int = len_as_var_int | least_significant_byte;

        length = length >> 7;
    }

    len_as_var_int = len_as_var_int << 8;
    len_as_var_int = len_as_var_int | (length as u64);

    len_as_var_int
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{
        CancelInvocation, Close, Completion, Invocation, ResultKind, StreamInvocation, StreamItem,
    };
    use std::collections::HashMap;

    #[test]
    fn write_blocking_invocation() {
        let invocation = Invocation {
            headers: HashMap::new(),
            id: Some("xyz".into()),
            target: "method".into(),
            arguments: vec![rmpv::Value::from(42)],
            stream_ids: None,
        };
        let expected = vec![
            0x95, 0x01, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
            0x91, 0x2a, 0x90,
        ];

        let mut buf = Vec::new();
        write_standard_invocation(&mut buf, invocation).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_non_blocking_invocation() {
        let invocation = Invocation {
            headers: HashMap::new(),
            id: None,
            target: "method".into(),
            arguments: vec![rmpv::Value::from(42)],
            stream_ids: None,
        };
        let expected = vec![
            0x95, 0x01, 0x80, 0xc0, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x91, 0x2a, 0x90,
        ];

        let mut buf = Vec::new();
        write_standard_invocation(&mut buf, invocation).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_blocking_invocation_with_stream_ids() {
        let invocation = Invocation {
            headers: HashMap::new(),
            id: Some("xyz".into()),
            target: "method".into(),
            arguments: vec![rmpv::Value::from(42)],
            stream_ids: Some(vec!["1".into()]),
        };
        let expected = vec![
            0x95, 0x01, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
            0x91, 0x2a, 0x91, 0xa1, 0x31,
        ];

        let mut buf = Vec::new();
        write_standard_invocation(&mut buf, invocation).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_stream_invocation() {
        let invocation = StreamInvocation {
            headers: HashMap::new(),
            id: "xyz".into(),
            target: "method".into(),
            arguments: vec![rmpv::Value::from(42)],
            stream_ids: None,
        };
        let expected = vec![
            0x95, 0x04, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
            0x91, 0x2a, 0x90,
        ];

        let mut buf = Vec::new();
        super::write_stream_invocation(&mut buf, invocation.into()).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_stream_item() {
        let stream_item = StreamItem {
            headers: HashMap::new(),
            invocation_id: "xyz".into(),
            item: rmpv::Value::from(42),
        };
        let expected = vec![0x94, 0x02, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x2a];

        let mut buf = Vec::new();
        super::write_stream_item(&mut buf, stream_item).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_completion_error_result() {
        let completion = Completion {
            headers: HashMap::new(),
            invocation_id: "xyz".into(),
            result_kind: ResultKind::Error,
            result: Some(rmpv::Value::from("Error")),
            error: None,
        };
        let expected = vec![
            0x95, 0x03, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x01, 0xa5, 0x45, 0x72, 0x72, 0x6f, 0x72,
        ];

        let mut buf = Vec::new();
        write_completion(&mut buf, completion).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_completion_void_result() {
        let completion = Completion {
            headers: HashMap::new(),
            invocation_id: "xyz".into(),
            result_kind: ResultKind::Void,
            result: None,
            error: None,
        };
        let expected = vec![0x94, 0x03, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x02];

        let mut buf = Vec::new();
        write_completion(&mut buf, completion).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_completion_non_void_result() {
        let completion = Completion {
            headers: HashMap::new(),
            invocation_id: "xyz".into(),
            result_kind: ResultKind::NonVoid,
            result: Some(rmpv::Value::from(42)),
            error: None,
        };
        let expected = vec![0x95, 0x03, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x03, 0x2a];

        let mut buf = Vec::new();
        write_completion(&mut buf, completion).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_cancel_invocation() {
        let cancel_invocation = CancelInvocation {
            headers: HashMap::new(),
            invocation_id: "xyz".into(),
        };
        let expected = vec![0x93, 0x05, 0x80, 0xa3, 0x78, 0x79, 0x7a];

        let mut buf = Vec::new();
        super::write_cancel_invocation(&mut buf, cancel_invocation).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_ping() {
        let expected = vec![0x91, 0x06];

        let mut buf = Vec::new();
        super::write_ping(&mut buf).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_close_with_error() {
        let close = Close {
            error: Some("xyz".into()),
            allow_reconnect: None,
        };
        let expected = vec![0x92, 0x07, 0xa3, 0x78, 0x79, 0x7a];

        let mut buf = Vec::new();
        write_close(&mut buf, close).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_close_with_allow_reconnect() {
        let close = Close {
            error: Some("xyz".into()),
            allow_reconnect: Some(true),
        };
        let expected = vec![0x93, 0x07, 0xa3, 0x78, 0x79, 0x7a, 0xc3];

        let mut buf = Vec::new();
        write_close(&mut buf, close).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn write_close_no_error() {
        let close = Close {
            error: None,
            allow_reconnect: None,
        };
        let expected = vec![0x92, 0x07, 0xa0];

        let mut buf = Vec::new();
        write_close(&mut buf, close).unwrap();

        assert_eq!(expected, buf);
    }

    #[test]
    fn convert_5248_to_var_int() {
        let expected = 0b1000000000101001;
        let actual = convert_to_var_int(5248);

        assert_eq!(expected, actual);
    }

    #[test]
    fn convert_53_to_var_int() {
        let expected = 0b00110101;
        let actual = convert_to_var_int(53);

        assert_eq!(expected, actual);
    }

    #[test]
    fn write_frame() {
        let buffer = vec![0x1, 0x2, 0x3];
        let expected = vec![0x3, 0x1, 0x2, 0x3];

        let mut actual = Vec::new();

        super::write_frame(&mut actual, buffer).unwrap();

        assert_eq!(expected, actual);
    }
}
