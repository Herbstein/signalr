use crate::{Message, SignalRCommunicator};
use std::io::{Read, Write};

mod read;
mod write;

pub struct SignalRMessagePack;

impl SignalRCommunicator for SignalRMessagePack {
    fn read<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
        let message_length = read_var_int(r)?;
        let mut message = r.take(message_length);
        read::read(&mut message)
    }

    fn write<W: Write>(w: &mut W, message: Message) -> Result<(), Box<dyn std::error::Error>> {
        write::write(w, message)
    }
}

fn read_var_int<R: Read>(r: &mut R) -> Result<u64, Box<dyn std::error::Error>> {
    let mut length_end = false;
    let mut var_int_bytes: Vec<u8> = Vec::new();
    while !length_end {
        let mut message_length_buffer = [0 as u8; 1];
        r.read_exact(&mut message_length_buffer)?;
        let temp = message_length_buffer.first().unwrap();
        var_int_bytes.insert(0, *temp);
        if temp >> 7 == 0 {
            length_end = true;
        }
    }

    let message_length = var_int_bytes.iter().fold(0 as u64, |mut acc, x| {
        let least_significant_bits = (x & 0b01111111) as u64;
        acc = acc << 7 | least_significant_bits;
        acc
    });

    Ok(message_length)
}

#[cfg(test)]
mod tests {
    use super::read_var_int;
    use std::io::Cursor;

    #[test]
    fn read_var_int_53() {
        let buffer = vec![0x35];
        let mut cursor = Cursor::new(buffer);

        let actual = read_var_int(&mut cursor).unwrap();

        assert_eq!(53, actual);
    }

    #[test]
    fn read_var_int_5248() {
        let buffer = vec![0x80, 0x29];
        let mut cursor = Cursor::new(buffer);

        let actual = read_var_int(&mut cursor).unwrap();

        assert_eq!(5248, actual);
    }
}
