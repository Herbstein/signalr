use crate::{
    CancelInvocation, Close, Completion, Invocation, Message, MessageType, ResultKind,
    StreamInvocation, StreamItem,
};
use rmp::decode;
use std::{collections::HashMap, convert::TryInto, io::Read};

pub(super) fn read<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let message_length = decode::read_array_len(r)?;
    if message_length < 1 {
        return Err("Message not long enough to determine type".into());
    }

    let message_type: u8 = decode::read_int(r)?;
    match message_type.try_into()? {
        MessageType::Invocation => read_invocation(r),
        MessageType::StreamInvocation => read_stream_invocation(r),
        MessageType::StreamItem => read_stream_item(r),
        MessageType::Completion => read_completion(r),
        MessageType::CancelInvocation => read_cancel_invocation(r),
        MessageType::Ping => Ok(Message::Ping),
        MessageType::Close => read_close(r),
    }
}

fn read_invocation<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let headers = read_headers(r).unwrap();

    let invocation_value = rmpv::decode::read_value(r)?;
    let invocation_id = match invocation_value {
        rmpv::Value::Nil => None,
        rmpv::Value::String(s) => s.into_str(),
        _ => {
            return Err("error when decoding null-or-string".into());
        }
    };

    let target = read_string(r)?;
    let arguments = read_arguments(r)?;
    let stream_ids = read_stream_ids(r)?;

    Ok(Message::Invocation(Invocation {
        headers,
        target,
        arguments,
        id: invocation_id,
        stream_ids,
    }))
}

fn read_stream_invocation<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let headers = read_headers(r).unwrap();
    let invocation_id = read_string(r)?;
    let target = read_string(r)?;
    let arguments = read_arguments(r)?;
    let stream_ids = read_stream_ids(r)?;

    Ok(Message::StreamInvocation(StreamInvocation {
        headers,
        target,
        arguments,
        id: invocation_id,
        stream_ids,
    }))
}

fn read_stream_item<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let headers = read_headers(r).unwrap();
    let invocation_id = read_string(r)?;
    let item = rmpv::decode::read_value(r)?;

    Ok(Message::StreamItem(StreamItem {
        invocation_id: invocation_id,
        headers,
        item,
    }))
}

fn read_completion<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let headers = read_headers(r).unwrap();
    let invocation_id = read_string(r)?;
    let result_kind = decode::read_pfix(r)?;

    let result = match result_kind.try_into()? {
        ResultKind::Error | ResultKind::NonVoid => Some(rmpv::decode::read_value(r).unwrap()),
        ResultKind::Void => None,
    };

    Ok(Message::Completion(Completion {
        invocation_id: invocation_id,
        headers,
        result_kind: result_kind.try_into()?,
        result,
        error: None,
    }))
}

fn read_cancel_invocation<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let headers = read_headers(r).unwrap();
    let invocation_id = read_string(r)?;

    Ok(Message::CancelInvocation(CancelInvocation {
        headers,
        invocation_id: invocation_id,
    }))
}

fn read_close<R: Read>(r: &mut R) -> Result<Message, Box<dyn std::error::Error>> {
    let error = if let Ok(value) = read_string(r) {
        Some(value)
    } else {
        None
    };
    let allow_reconnect = if let Ok(value) = decode::read_bool(r) {
        Some(value)
    } else {
        None
    };

    Ok(Message::Close(Close {
        error,
        allow_reconnect,
    }))
}

fn read_headers<R: Read>(r: &mut R) -> Result<HashMap<String, String>, Box<dyn std::error::Error>> {
    let headers_length = decode::read_map_len(r)?;
    let mut headers = HashMap::new();
    for _ in 0..headers_length {
        let key = match read_string(r) {
            Ok(v) => v,
            Err(_) => return Err("unknown error when parsing key".into()),
        };

        let value = match read_string(r) {
            Ok(v) => v,
            Err(_) => return Err("unknown error when parsing value".into()),
        };

        headers.insert(key, value);
    }

    Ok(headers)
}

fn read_arguments<R: Read>(r: &mut R) -> Result<Vec<rmpv::Value>, Box<dyn std::error::Error>> {
    let arguments_length = decode::read_array_len(r)?;
    let mut arguments = Vec::with_capacity(arguments_length as usize);
    for _ in 0..arguments_length {
        let value = rmpv::decode::read_value(r)?;
        arguments.push(value);
    }

    Ok(arguments)
}

fn read_stream_ids<R: Read>(r: &mut R) -> Result<Option<Vec<String>>, Box<dyn std::error::Error>> {
    let stream_ids_length = decode::read_array_len(r)?;
    if stream_ids_length == 0 {
        Ok(None)
    } else {
        let mut stream_ids = Vec::with_capacity(stream_ids_length as usize);
        for _ in 0..stream_ids_length {
            let value = read_string(r)?;
            stream_ids.push(value);
        }

        Ok(Some(stream_ids))
    }
}

fn read_string<R: Read>(r: &mut R) -> Result<String, Box<dyn std::error::Error>> {
    let string_value = rmpv::decode::read_value(r)?;

    if let rmpv::Value::String(s) = string_value {
        if let Some(value) = s.into_str() {
            Ok(value)
        } else {
            Err("unknown error parsing string value.".into())
        }
    } else {
        Err("unknown error parsing string value.".into())
    }
}

#[cfg(test)]
mod tests {
    use super::read;
    use crate::{Close, Message, ResultKind};
    use std::collections::HashMap;
    use std::io::Cursor;

    #[test]
    fn message_pack_invocation() {
        let message = vec![
            0x95, 0x01, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
            0x91, 0x2a, 0x90,
        ];
        let mut cursor = Cursor::new(message);

        if let Message::Invocation(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!(Some("xyz".into()), parsed_message.id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!("method".to_string(), parsed_message.target);
            assert_eq!(vec![rmpv::Value::from(42)], parsed_message.arguments);
        };
    }

    #[test]
    fn message_pack_invocation_with_headers() {
        let message = vec![
            0x95, 0x01, 0x82, 0xa1, 0x78, 0xa1, 0x79, 0xa1, 0x7a, 0xa1, 0x7a, 0xa3, 0x78, 0x79,
            0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64, 0x91, 0x2a, 0x90,
        ];
        let mut headers = HashMap::new();
        headers.insert("x".to_string(), "y".to_string());
        headers.insert("z".to_string(), "z".to_string());

        let mut cursor = Cursor::new(message);

        if let Message::Invocation(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!(Some("xyz".into()), parsed_message.id);
            assert_eq!(headers, parsed_message.headers);
            assert_eq!("method".to_string(), parsed_message.target);
            assert_eq!(vec![rmpv::Value::from(42)], parsed_message.arguments);
        };
    }

    #[test]
    fn message_pack_invocation_with_stream_ids() {
        let message = vec![
            0x95, 0x01, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
            0x91, 0x2a, 0x91, 0xa1, 0x31,
        ];
        let mut cursor = Cursor::new(message);

        if let Message::Invocation(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!(Some("xyz".into()), parsed_message.id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!("method".to_string(), parsed_message.target);
            assert_eq!(vec![rmpv::Value::from(42)], parsed_message.arguments);
            assert_eq!(Some(vec!["1".to_string()]), parsed_message.stream_ids);
        };
    }

    #[test]
    fn message_pack_stream_invocation() {
        let message = vec![
            0x95, 0x04, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0xa6, 0x6d, 0x65, 0x74, 0x68, 0x6f, 0x64,
            0x91, 0x2a, 0x90,
        ];
        let mut cursor = Cursor::new(message);

        if let Message::StreamInvocation(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!("xyz".to_string(), parsed_message.id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!("method".to_string(), parsed_message.target);
            assert_eq!(vec![rmpv::Value::from(42)], parsed_message.arguments);
        };
    }

    #[test]
    fn message_pack_stream_item() {
        let message = vec![0x94, 0x02, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x2a];
        let mut cursor = Cursor::new(message);

        if let Message::StreamItem(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!("xyz".to_string(), parsed_message.invocation_id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!(rmpv::Value::from(42), parsed_message.item);
        };
    }

    #[test]
    fn message_pack_completion_error_result() {
        let message = vec![
            0x95, 0x03, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x01, 0xa5, 0x45, 0x72, 0x72, 0x6f, 0x72,
        ];
        let mut cursor = Cursor::new(message);

        if let Message::Completion(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!("xyz".to_string(), parsed_message.invocation_id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!(ResultKind::Error, parsed_message.result_kind);
            assert_eq!(
                Some(rmpv::Value::from("Error".to_string())),
                parsed_message.result
            );
        };
    }

    #[test]
    fn message_pack_completion_void_result() {
        let message = vec![0x94, 0x03, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x02];
        let mut cursor = Cursor::new(message);

        if let Message::Completion(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!("xyz".to_string(), parsed_message.invocation_id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!(ResultKind::Void, parsed_message.result_kind);
            assert_eq!(None, parsed_message.result);
        };
    }

    #[test]
    fn message_pack_completion_non_void_result() {
        let message = vec![0x95, 0x03, 0x80, 0xa3, 0x78, 0x79, 0x7a, 0x03, 0x2a];
        let mut cursor = Cursor::new(message);

        if let Message::Completion(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!("xyz".to_string(), parsed_message.invocation_id);
            assert_eq!(HashMap::new(), parsed_message.headers);
            assert_eq!(ResultKind::NonVoid, parsed_message.result_kind);
            assert_eq!(Some(rmpv::Value::from(42)), parsed_message.result);
        };
    }

    #[test]
    fn message_pack_cancel_invocation() {
        let message = vec![0x93, 0x05, 0x80, 0xa3, 0x78, 0x79, 0x7a];
        let mut cursor = Cursor::new(message);

        if let Message::CancelInvocation(parsed_message) = read(&mut cursor).unwrap() {
            assert_eq!("xyz".to_string(), parsed_message.invocation_id);
            assert_eq!(HashMap::new(), parsed_message.headers);
        };
    }

    #[test]
    fn message_pack_ping() {
        let message = vec![0x91, 0x06];
        let mut cursor = Cursor::new(message);

        if let Message::Ping = read(&mut cursor).unwrap() {
            assert!(true);
        }
    }

    #[test]
    fn message_pack_close() {
        let message = vec![0x92, 0x07, 0xa0];
        let mut cursor = Cursor::new(message);

        let expected = Message::Close(Close {
            error: Some("".into()),
            allow_reconnect: None,
        });

        let message = read(&mut cursor).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn message_pack_close_with_error() {
        let message = vec![0x92, 0x07, 0xa3, 0x78, 0x79, 0x7a];
        let mut cursor = Cursor::new(message);

        let expected = Message::Close(Close {
            error: Some("xyz".into()),
            allow_reconnect: None,
        });

        let message = read(&mut cursor).unwrap();

        assert_eq!(expected, message);
    }

    #[test]
    fn message_pack_close_with_allow_reconnect() {
        let message = vec![0x92, 0x07, 0xa0, 0xc3];
        let mut cursor = Cursor::new(message);

        let expected = Message::Close(Close {
            error: Some("".into()),
            allow_reconnect: Some(true),
        });

        let message = read(&mut cursor).unwrap();

        assert_eq!(expected, message);
    }
}
